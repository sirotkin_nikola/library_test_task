from django.urls import path, include
from library_app.views import *
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView, TokenVerifyView, )


urlpatterns = [
    path('event/', EventView.as_view(), name='event'),
    path('organization/', OrganizationView.as_view(), name='organizations'),
    path('events_list/', EventListView.as_view(), name='event_list'),
    path('events_list/<int:pk>', OneEventViewSet.as_view()),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),
]
