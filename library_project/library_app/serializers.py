from .models import *
from rest_framework.serializers import ModelSerializer
from rest_framework import serializers


class EventSerializer(ModelSerializer):
    class Meta:
        model = Event
        fields = "__all__"


class OrganizationSerializer(ModelSerializer):
    class Meta:
        model = Organization
        fields = "__all__"


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = "__all__"