import time
from celery import Celery
from decouple import config


app = Celery('myapp', broker=f'{config("REDIS_HOST")}://{config("REDIS_HOST")}:{config("REDIS_PORT")}/0')


@app.task
def sleep_function():
    time.sleep(60)
