from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from library_app.models import User, Event, Organization
from django.utils.safestring import mark_safe


class UsersAdmin(admin.ModelAdmin):
    list_display = ['id', 'is_superuser', 'username', 'is_staff',
                    'is_active', 'date_joined', 'email', 'phone', 'user_organizations']


admin.site.register(User, UsersAdmin)


class EventAdmin(admin.ModelAdmin):
    list_display = ['title', 'description', 'image_preview', 'date', 'image']
    list_filter = ('date',)
    search_fields = ('title', 'date')

    def image_preview(self, obj):
        return mark_safe(f'<img src="{obj.image.url}" style="max-width:100px; max-height:100px;" />')

    image_preview.short_description = 'Preview'


admin.site.register(Event, EventAdmin)


class OrganizationAdmin(admin.ModelAdmin):
    def get_list_display(self, request):
        return [field.name for field in self.model._meta.concrete_fields]

    list_filter = ('address', 'postcode')
    search_fields = ('title', 'address', 'postcode')


admin.site.register(Organization, OrganizationAdmin)
