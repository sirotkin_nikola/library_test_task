from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework import status
from .models import *
from .serializers import *
from rest_framework import generics, filters
from rest_framework.filters import OrderingFilter, SearchFilter
from django_filters.rest_framework import DjangoFilterBackend
from library_app.paginations import *
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import CreateAPIView
from .tasks import *


class EventView(CreateAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        sleep_function.delay()
        return self.create(request, *args, **kwargs)


class OrganizationView(CreateAPIView):
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer
    permission_classes = (IsAuthenticated,)


class EventListView(generics.ListAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter, SearchFilter]
    filterset_fields = ['date']
    ordering_fields = ['date']
    search_fields = ['title']
    pagination_class = EventAPIListPagination
    permission_classes = (IsAuthenticated,)


class OneEventViewSet(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, pk):
        event_with_users = Event.objects.prefetch_related('organizations').get(id=pk)
        organizations = event_with_users.organizations.all()
        organization_data = []
        for organization in organizations:
            users = User.objects.select_related('user_organizations').filter(user_organizations=organization).all()
            serialized_users = UserSerializer(users, many=True).data
            organization_data.append({
                'organization': OrganizationSerializer(organization).data,
                'users': serialized_users
            })

        return Response({'code': status.HTTP_200_OK, 'result': organization_data})
