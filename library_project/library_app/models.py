from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.core.validators import RegexValidator


class CustomUserManager(BaseUserManager):
    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if not email:
            raise ValueError('The Email field must be set')

        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user


class Organization(models.Model):
    title = models.CharField(max_length=200, verbose_name='название организации')
    description = models.TextField(verbose_name='описание мероприятия')
    address = models.CharField(max_length=200, verbose_name='адрес мероприятия')
    postcode = models.CharField(max_length=10, verbose_name='код')

    class Meta:
        verbose_name = 'Организация'
        verbose_name_plural = 'Организации'

    def __str__(self):
        return self.title


class Event(models.Model):
    title = models.CharField(max_length=200, verbose_name='название меприятия')
    description = models.TextField(verbose_name='описание мероприятия')
    organizations = models.ManyToManyField(Organization)
    image = models.ImageField(upload_to='images/')
    date = models.DateField(verbose_name='дата мероприятия')

    class Meta:
        verbose_name = 'Мероприятие'
        verbose_name_plural = 'Мероприятии'

    def __str__(self):
        return self.title


class User(AbstractUser):
    email = models.EmailField(verbose_name='email', unique=True)
    phone_regex = RegexValidator(regex=r'^\+7\d{10}$',
                                 message="Incorrect phone number input, correct format: +79999999999")
    phone = models.CharField(validators=[phone_regex], max_length=17)
    user_organizations = models.OneToOneField(Organization, on_delete=models.SET_NULL,
                                              verbose_name='организация', null=True)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return self.email
