from django.apps import AppConfig


class LibraryChatAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'library_chat_app'
